package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_web.view.*
import kotlinx.android.synthetic.main.fragment_web.view.bottom_nav_bar


class WebFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_web, container, false)

        val navController = NavHostFragment.findNavController(this)

        fragmentLayout.bottom_nav_bar.setupWithNavController(navController)
        fragmentLayout.bottom_nav_bar.setOnNavigationItemSelectedListener { item ->
            navController.navigate(item.itemId)
            return@setOnNavigationItemSelectedListener true
        }
        // Inflate the layout for this fragment
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() = WebFragment()
    }
}
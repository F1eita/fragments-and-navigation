package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_registration.view.*


class RegistrationFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_registration, container, false)

        val navController = NavHostFragment.findNavController(this)

        fragmentLayout.login.setOnClickListener { navController.navigate(R.id.loginFragment) }
        // Inflate the layout for this fragment
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}
package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavHostController
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_login.view.*


class LoginFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_login, container, false)

        val navController = NavHostFragment.findNavController(this)


        fragmentLayout.signIn.setOnClickListener{ navController.navigate(R.id.homeFragment)}
        fragmentLayout.signUp.setOnClickListener { navController.navigate(R.id.registrationFragment) }
        // Inflate the layout for this fragment
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}